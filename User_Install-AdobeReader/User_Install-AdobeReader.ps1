<#
.SYNOPSIS
  Installs Adobe Reader on target machine

.DESCRIPTION
  This script will copy Acrobat reader down from an Azure Blob to "C:\Innit Software Distribution" and install it using the required arguments. 
  It will then delete the file from the folder. 

.INPUTS
  $CheckIfInstalledPath - path to exe to check if program already exists.
  $Uri - Azure blob Uri of installer
  $SoftwareName - full name of installer file
  $FolderPath - leave as is
  $scriptName - name of the script, used for logging - keep formatting please.
  $Logfolder - leave as is
  $Dlpath - leave as is

.NOTES
    Name:           User_Install-AdobeReader
    Version:        1.0
    Author:         Mikael Lognseth @ Innit Drift AS
    Creation Date:  03.11.2020
    Purpose/Change: Initial script development

#>

#Variabler:
$CheckIfInstalledPath = "C:\Program Files (x86)\Adobe\Acrobat Reader DC\Reader\AcroRd32.exe";
$Uri = "https://innitdevicemgmt.blob.core.windows.net/allkopi/Applikasjoner/AcroRdrDC1800920044_nb_NO.exe";
$SoftwareName = "AcroRdrDC1800920044_nb_NO.exe";
$FolderPath = "C:\Innit\";
$scriptName = 'User_Install-AdobeReader';
$logFolder = 'C:\Innit\00_logs';
$DlPath = $FolderPath + $SoftwareName;
$Arguments = "/msi EULA_ACCEPT=YES REMOVE_PREVIOUS=YES /qn"

$logPath = Join-Path $logFolder "$scriptName.txt"; 

Start-Transcript -Path $logPath -Append; 

if (!(Test-Path $FolderPath)) {
    New-Item -ItemType Directory -Path $FolderPath;
}

##Sjekk om maskinen allerede har installert software.
if (Test-Path -Path $CheckIfInstalledPath){
  Write-Host "$SoftwareName already installed";
} 
else {
  Set-Location -Path $FolderPath;
  Invoke-WebRequest -Uri $Uri -OutFile $DlPath;
  $Install = $SoftwareName;
  
  #Install
  $cmd=".\$Install " + $Arguments;
  $outputVar=Invoke-Expression $cmd;
  Write-Host -Object ("Output from installer: {0}" -f ($outputVar));

#Remove installation file 

  $Count = 0
  DO {
    if ($Count -eq 10) {break}
    Remove-Item -Path $Dlpath -Recurse -Force -ErrorAction Continue;
    Start-Sleep -Seconds 5
    $Count++
    } while ($Count -le 10)
  }     

Stop-Transcript;