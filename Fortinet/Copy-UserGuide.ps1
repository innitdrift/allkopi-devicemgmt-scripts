$ProgressPreference = 'SilentlyContinue'

$Uri = "https://allkopidevicemgmt.blob.core.windows.net/applications/Fortinet_VPN/Veiledning%20for%20konfigurasjon%20av%20Fortinet%20VPN%20AKNP.pdf";
$Outpath = "C:\Users\Public\Desktop\";
$FileName = "Veiledning for konfigurasjon av Fortinet VPN AKNP.pdf";
$OutFile = $Outpath + $FileName;

Invoke-WebRequest -Uri $Uri -OutFile $OutFile;

if (Test-Path -Path $OutFile) {
    Write-Host("Successfully copied user guide to Public Desktop")
}