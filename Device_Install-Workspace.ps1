$storageUrl = "https://innitdevicemgmt.blob.core.windows.net/allkopi/CitrixWorkspaceApp.exe";
$DestFolder = "C:\Innit\Installers";
$azcopyBlobUrl = "https://innitdevicemgmt.blob.core.windows.net/backe/Apps/azcopy.exe";
$sasToken = "?sv=2018-03-28&si=backe-1717F7F5576&sr=c&sig=mKrEOC28OU8HTHQaoClc5N7voBjQclxAVSkDixJyW58%3D";
$scriptPath = "C:\innit\Scripts\";
$azcopyFilenName = "azcopy.exe";

if (Test-Path -Path "C:\Program Files (x86)\Citrix\ICA Client"){
    Write-Host "Already exists, closing."
}
else {
    if (!$DestFolder) {
        mkdir $DestFolder
    }
    
    #Copy AZCopy to device
    $azcopyUrl = $azcopyBlobUrl + $sasToken;
    $azcopyOutPath = $scriptPath + $azcopyFilenName;

    Invoke-WebRequest -Uri $azcopyUrl -OutFile $azcopyOutPath;

    #Copy down Install file
    $cmd="c:\innit\Scripts\azcopy.exe sync "+$storageUrl+" "+$destFolder;
    $outputVar=Invoke-Expression $cmd;
    Write-Host -Object ("Output from azcopy: {0}" -f ($outputVar))

    Set-Location -Path $DestFolder 
    $install = "CitrixWorkspaceApp.exe /silent /norestart";
    $outVar = Invoke-Expression $install;
    Write-Host -Object ("Output from Citrix install {0}" -f ($outVar))
}