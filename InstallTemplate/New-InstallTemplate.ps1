<#
.SYNOPSIS
  Generic install script getting arguments from N Central

.DESCRIPTION
  Install script that gets arguments passed from N Central to install given software on target machine.

.INPUTS
  $CheckIfInstalledPath - path to exe to check if program already exists.
  $Uri - Azure blob Uri of installer
  $SoftwareName - full name of installer file
  $FolderPath - leave as is
  $scriptName - name of the script, used for logging - keep formatting please.
  $Logfolder - leave as is
  $Dlpath - leave as is
  $Arguments - install arguments

.NOTES
    Name:           InstallTemplate
    Version:        1.0
    Author:         Mikael Lognseth @ Innit Drift AS
    Creation Date:  04.11.2020
    Purpose/Change: Initial script development
#>

##Variables that you have to set if running outside of N-Central:
<#
$CheckIfInstalledPath = 
$Uri = 
$SoftwareName = 
$scriptName = 
$Arguments = 
#>

##LEAVE THESE VARIABLES AS IS
$FolderPath = "C:\Innit\";
$logFolder = 'C:\Innit\00_logs';
$DlPath = $FolderPath + $SoftwareName;
$logPath = Join-Path $logFolder "$scriptName.txt"; 
$ProgressPreference = ‘SilentlyContinue’

$Minutes = $Sleeptimer -as [int]
$Seconds = $Minutes * 60

Start-Sleep -Seconds $Seconds

Start-Transcript -Path $logPath -Append; 

if (!(Test-Path $FolderPath)) {
    New-Item -ItemType Directory -Path $FolderPath;
}

##Sjekk om maskinen allerede har installert software.
if (Test-Path -Path $CheckIfInstalledPath){
  Write-Host "$SoftwareName already installed";
} 
else {
  Set-Location -Path $FolderPath;
  Invoke-WebRequest -Uri $Uri -OutFile $DlPath;
  $Install = $SoftwareName;
  
  #Install
  $cmd=".\$Install " + $Arguments;
  $outputVar=Invoke-Expression $cmd;
  Write-Host -Object ("Output from installer: {0}" -f ($outputVar));
}

  #Remove installation file 
DO {
  if (!(Test-Path $Dlpath)) {break}
  if ($CheckIfInstalledPath) {
    Remove-Item -Path $Dlpath -Recurse -Force -ErrorAction SilentlyContinue;
    Start-Sleep -Seconds 5
  }
} until (!(Test-Path $Dlpath))

if (!(Test-Path $Dlpath)) {
  Write-Host("Successfully removed installation files for $SoftwareName") -ForegroundColor Green
}

Stop-Transcript;