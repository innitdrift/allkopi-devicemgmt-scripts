#Variabler:
$username = $ENV:UserProfile
$CheckIfInstalledPath = "$username\AppData\Local\Microsoft\Teams\Current\Teams.exe"
$32BitUri = "https://statics.teams.microsoft.com/production-windows/1.1.00.5855/Teams_windows.exe"
$64BitUri = "https://statics.teams.microsoft.com/production-windows/1.1.00.5855/Teams_windows_x64.exe"
$SoftwareName =
$32BitPath = "$username\Downloads\Teams_Windows.exe"
$64BitPath = "$username\Downloads\Teams_Windows_x64.exe" 

##Sjekk om maskinen allerede har installert software.
if (Test-Path -Path $CheckIfInstalledPath){

        Write-Host "$SoftwareName already installed"
    
} 
else {

    if ([System.IntPtr]::Size -eq 4) {

            Write-Host "System is 32-bit"
            Invoke-WebRequest -Uri $32BitUri -OutFile $32BitPath
            $Install = $32BitPath
            

         } else { 
            Invoke-WebRequest -Uri $64BitUri -OutFile $64BitPath
            $Install = $64BitPath
            Write-Host "System is 64-bit"
          }

          #Install
          & $Install -s
}
